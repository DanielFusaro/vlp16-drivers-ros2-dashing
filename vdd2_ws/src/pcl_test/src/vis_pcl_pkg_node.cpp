#include <iostream>
#include <memory>
#include "std_msgs/msg/string.hpp"


#include "rclcpp/rclcpp.hpp"

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/filters/filter.h>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/io/io.h>
#include <pcl/common/common_headers.h>

#include <pcl_conversions/pcl_conversions.h>
#include <pcl/common/common.h>

#include "sensor_msgs/msg/point_cloud2.hpp"

using std::placeholders::_1;


pcl::PointCloud<pcl::PointXYZI>::Ptr laserCloudIn;

pcl::visualization::CloudViewer viewer ("Simple Cloud Viewer");
std::string pointCloudTopic = "/velodyne_points";

class Subscriber : public rclcpp::Node
{
  public:
    Subscriber()
    : Node("subscriber")
    {
      subLaserCloud = this->create_subscription<sensor_msgs::msg::PointCloud2>(
      pointCloudTopic, 10, std::bind(&Subscriber::cloudHandler, this, std::placeholders::_1));
      
      std::cout << "\033[1;32m---->\033[0m Subscriber started." << std::endl;
    }

  private:
    void cloudHandler(const sensor_msgs::msg::PointCloud2::SharedPtr laserCloudMsg) const
    {
		std::cout << "Received!" << std::endl;
		laserCloudIn.reset(new pcl::PointCloud<pcl::PointXYZI>());
		laserCloudIn->clear();
		pcl::fromROSMsg(*laserCloudMsg, *laserCloudIn);

		std::vector<int> indices;

		pcl::removeNaNFromPointCloud(*laserCloudIn, *laserCloudIn, indices);
		viewer.showCloud(laserCloudIn);
    }
    rclcpp::Subscription<sensor_msgs::msg::PointCloud2>::SharedPtr subLaserCloud;
};

int main(int argc, char **argv) {

	rclcpp::init(argc, argv);	


	rclcpp::spin(std::make_shared<Subscriber>());
	rclcpp::shutdown();

	return 0;
}