# ROS DASHING VELODYNE DRIVERS
## dependencies
 * ubuntu 18.04 LTS ( but ubuntu 20.04 _should_ work the same)
 * ros2 dashing
 * rosdep
 	* sudo apt-get install python3-rosdep

1) install ros dependencies

	sudo apt-get install ros-dashing-velodyne

2) source ros2

	source /opt/ros/dashing/setup.bash

3) create ws & cd 

	mkdir -p velodyne_drivers_dashing/src
	cd velodyne_drivers_dashing/src

4) download velodyne drivers

	git clone https://github.com/ros-drivers/velodyne.git

5) checkout dashing branch

	cd velodyne
	git checkout origin/dashing-devel

6) update all dependencies
	
	cd ../../
	rosdep install --from-paths src --ignore-src --rosdistro dashing -y

7) build things
	
	colcon build

8) source the new setup

	source install/setup.bash


8.5) setup the IP
	http://wiki.ros.org/velodyne/Tutorials/Getting%20Started%20with%20the%20Velodyne%20VLP16


9) launch them!

	ros2 launch velodyne velodyne-all-nodes-VLP16-launch.py





### how to see the cloud
   * if you have lego_loam_sr
	  * `ros2 launch lego_loam_sr run.launch.py`
   * else
	  * `ros2 run pcl_test pcl_test_node`